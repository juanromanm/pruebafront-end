import { TestBed } from '@angular/core/testing';

import { View1Service } from './view1.service';

describe('View1Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: View1Service = TestBed.get(View1Service);
    expect(service).toBeTruthy();
  });
});
