import { Injectable } from '@angular/core';
import { Global } from './global';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class View1Service {
  url: string
  constructor(private _http: HttpClient) {
    this.url = Global.url;
  }
  getView1(): Observable<any> {
    return this._http.get(this.url + 'array.php');
  }
}
