import { TestBed } from '@angular/core/testing';

import { View2Service } from './view2.service';

describe('View2Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: View2Service = TestBed.get(View2Service);
    expect(service).toBeTruthy();
  });
});
