import { Injectable } from '@angular/core';
import { Global } from './global';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class View2Service {
  url: string
  constructor(private _http: HttpClient) {
    this.url = Global.url;
  }
  getView2(): Observable<any> {
    return this._http.get(this.url + 'dict.php');
  }
}
