import { Component, OnInit } from '@angular/core';
import { View1Service } from '../../services/view1.service';
import { View2Service } from '../../services/view2.service';
import {ResponseView} from '../../models/response';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [View1Service,View2Service]
})
export class HomeComponent implements OnInit {

  response1:ResponseView;
  constructor(private _view1Service: View1Service,private _view2Service: View2Service) { }

  ngOnInit() {
    this._view1Service.getView1().subscribe(
      response => {
        console.log("respuesta1", response);
        this.response1=response;
        console.log("data",this.response1.data);
      },
      error => {
        console.log(error);
      }
    );
    this._view2Service.getView2().subscribe(
      response => {
        console.log("respuesta2", response);
      },
      error => {
        console.log(error);
      }
    );
  }

}
