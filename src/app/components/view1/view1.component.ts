import { Component, OnInit } from '@angular/core';
import { View1Service } from '../../services/view1.service';
import {ResponseView} from '../../models/response';

@Component({
  selector: 'app-view1',
  templateUrl: './view1.component.html',
  styleUrls: ['./view1.component.css']
})
export class View1Component implements OnInit {

  response1:ResponseView;
  constructor(private _view1Service: View1Service) { }

  ngOnInit() {

  }

  get(){
    this._view1Service.getView1().subscribe(
      response => {
        console.log("respuesta2", response);
      },
      error => {
        console.log(error);
      }
    );
  }

}
