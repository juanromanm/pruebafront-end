import { Component, OnInit } from '@angular/core';
import { View2Service } from '../../services/view2.service';
import { ResponseView } from '../../models/response';

@Component({
  selector: 'app-view2',
  templateUrl: './view2.component.html',
  styleUrls: ['./view2.component.css']
})
export class View2Component implements OnInit {

  response2: ResponseView;
  paragraphs:any;
  constructor(private _view2Service: View2Service) { }

  rowLetter = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'ñ', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
  ngOnInit() {
    this.myFunction('e',0,0);
    const words = ['spray', 'limit', 'elite', 'exuberant', 'destruction', 'present'];

    words.map((word)=>{console.log(word)})
    
  }

 myFunction(paragraph,letra,indice,suma) {
   console.log("indice", indice)
    var n = paragraph.indexOf(letra, indice);
    console.log("letra",n);
    
    if(n===-1)
    {
    return suma
    }
    else
    {
    let indiceNew=n;
    suma=suma+1;
    console.log("sumando",suma);
    return this.myFunction(paragraph,letra,indiceNew+1,suma);
    }
    
  }

  get() {
    this._view2Service.getView2().subscribe(
      response => {
        /* console.log("respuesta2", response);
        console.log("data",response.data); */
        this.response2=response;
       this.response2.data=JSON.parse(this.response2.data);
       console.log('p', this.response2);
        /* datas.map((data)=>{
          console.log("PAR",JSON.parse(data).paragraph)
        }) */
      },
      error => {
        console.log(error);
      }
    );
  }
}
