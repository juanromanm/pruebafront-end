export class ResponseView {
    public data?: any;
    public error?: any;
    public success: boolean;


    constructor(data, error, success) {
        this.data = data;
        this.error = error;
        this.success = success;
    }
}